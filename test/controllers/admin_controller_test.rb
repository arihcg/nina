require 'test_helper'

class AdminControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get ban" do
    get :ban
    assert_response :success
  end

  test "should get makeAdmin" do
    get :makeAdmin
    assert_response :success
  end

  test "should get makeNormal" do
    get :makeNormal
    assert_response :success
  end

  test "should get makeManager" do
    get :makeManager
    assert_response :success
  end

  test "should get destroy" do
    get :destroy
    assert_response :success
  end

end
