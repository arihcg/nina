class CandidatesController < ApplicationController
  before_action :set_candidate, only: [:show, :edit, :update, :destroy]
  before_action :set_poll, only: [:new  ]

  before_filter :authenticate_manager_user!, except: [:index, :show]
  respond_to :html

  def index
    @candidates = Candidate.all
    respond_with(@candidates)
  end

  def show
    respond_with(@candidate)
  end

  def new
    if @poll.creator == current_user
      @candidate = Candidate.new
      @candidate.poll = @poll
      respond_with(@candidate)
    else
      flash[:alert] = "This area is restricted to creator only."
      redirect_to(:back)
    end
  end

  def edit
    if @candidate.poll.creator != current_user
      flash[:alert] = "This area is restricted to creator only."
      redirect_to(:back)
    end
  end

  def create
    @candidate = Candidate.new(candidate_params)
    if @candidate.poll.creator == current_user
      @candidate.save
      respond_with(@candidate)
    else
      flash[:alert] = "You can only add candidates from your polls."
      redirect_to(:back)
    end
  end

  def update
    if @candidate.poll.creator == current_user
      @candidate.update(candidate_params)
      respond_with(@candidate)
    else
      flash[:alert] = "You can only update candidates from your polls."
      redirect_to(:back)
    end
  end

  def destroy
    if @candidate.poll != nil and @candidate.poll.creator == current_user
      @candidate.destroy
      respond_with(@candidate)
    else
      flash[:alert] = "You can only destroy candidates from your polls."
      redirect_to(:back)
    end
  end

  private
    def set_candidate
      @candidate = Candidate.find(params[:id])
    end

    def candidate_params
      params.require(:candidate).permit(:name, :description, :poll_id, :link1, :link2)
    end

    def set_poll
      @poll = Poll.find(params[:poll_id])
    end
end
