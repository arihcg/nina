class ElectionController < ApplicationController
  before_action :set_poll, only: [:view]
  before_filter :authenticate_user!, except: [:index]

  respond_to :html
  def index
    @polls = Poll.all
  end

  def view

  end

  def vote

    if (!isStudent(current_user.login))
      flash[:alert] = "This poll is only for students."
      redirect_to election_view_path(@poll) and return
    end

    @candidate = Candidate.find(params[:candidate_id])
    @poll = @candidate.poll

    current_user.votes.each do |vote|
      if vote.poll == @poll
        flash[:alert] = "You have already voted."
        redirect_to election_view_path(@poll) and return
      end
    end

    vote = Vote.new(user: current_user, candidate: @candidate, poll: @poll)
    if vote.save and (@poll.update(participants: @poll.participants+1))
      flash[:notice] = "Your vote has been sucessfully recorded."
      UserMailer.voted(current_user, @poll).deliver
    else
      flash[:alert] = "Your vote has failed. Please contact Admin."
    end
    redirect_to election_view_path(@poll)
  end


  private
    def set_poll
      @poll = Poll.find(params[:poll_id])
    end

    def isStudent(id)
      return id.match(/[s][t]\d{6,}/) != nil
    end

    def is_number?
      self.to_f.to_s == self.to_s || self.to_i.to_s == self.to_s
    end

end
