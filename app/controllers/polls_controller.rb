class PollsController < ApplicationController
  before_action :set_poll, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_manager_user!, except: [:index]
  respond_to :html

  def index
    @polls = Poll.all
    respond_with(@polls)
  end

  def show
    @candidates = @poll.candidates
    respond_with(@poll)
  end

  def new
    @poll = Poll.new
    respond_with(@poll)
  end

  def edit
    if @poll.creator != current_user
      flash[:alert] = "You can only update your polls."
      redirect_to(polls_path)
    end
  end

  def create
    @poll = Poll.new(poll_params)
    @poll.creator = current_user
    @poll.save
    UserMailer.poll_created(current_user, @poll).deliver
    respond_with(@poll)
  end

  def update
    if @poll.creator == current_user
      @poll.update(poll_params)
    else
      flash[:alert] = "You can only update your polls."
    end
    respond_with(@poll)
  end

  def destroy
    if @poll.creator == current_user
      @poll.destroy
    else
      flash[:alert] = "You can only destroy your polls."
    end
    respond_with(@poll)
  end

  private
    def set_poll
      @poll = Poll.find(params[:id])
    end

    def poll_params
      params.require(:poll).permit(:title, :description, :start, :end, :participants)
    end
end
