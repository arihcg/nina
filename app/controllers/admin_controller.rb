class AdminController < ApplicationController

  before_action :set_user, only: [:destroy, :makeAdmin, :makeNormal, :makeManager, :ban, :release]
  before_filter :authenticate_admin_user!

  def index
    @users = User.all
    @admins = User.where(role_id: 2)
    @managers = User.where(role_id: 3)
  end

  def ban
    @user.update(banned: true)
    flash[:notice] = "User sucessfully banned."
    redirect_to admin_path
  end

  def release
    @user.update(banned: false)
    flash[:notice] = "User sucessfully released."
    redirect_to admin_path
  end

  def makeAdmin
    @user.update(role_id: 2)
    redirect_to admin_path
  end

  def makeNormal
    @user.update(role_id: 1)
    redirect_to admin_path
  end

  def makeManager
    @user.update(role_id: 3)
    redirect_to admin_path
  end

  def destroy
    @user.destroy
    flash[:notice] = "User sucessfully destroyed."
    redirect_to admin_path
  end

  private
  def set_user
    @user = User.find(params[:id]) if params[:id]
  end
end
