class Vote < ActiveRecord::Base
  belongs_to :candidate
  belongs_to :user
  belongs_to :poll
end
