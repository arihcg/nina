class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :ldap_authenticatable, :rememberable, :trackable

  has_many :polls, foreign_key: 'creator_id', dependent: :destroy
  has_many :votes

  belongs_to :role

  def admin?
    self.role_id == 2
  end

  def normal?
    self.role_id == 1
  end

  def manager?
    self.role_id == 3
  end

end
