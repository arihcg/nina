class Poll < ActiveRecord::Base
  has_many :candidates, dependent: :destroy
  has_many :votes

  belongs_to :creator, class_name: 'User'
end
