class UserMailer < ActionMailer::Base
  default from: "aitcomplaints@gmail.com"

  def poll_created(user, poll)
    @user = user
    @poll = poll
    mail(:to => user.login+'@ait.asia', :subject => "Poll created")
  end

  def voted(user, poll)
    @user = user
    @poll = poll
    mail(:to => user.login+'@ait.asia', :subject => "Poll created")
  end
end
