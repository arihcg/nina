json.array!(@polls) do |poll|
  json.extract! poll, :id, :title, :description, :start, :end, :participants
  json.url poll_url(poll, format: :json)
end
