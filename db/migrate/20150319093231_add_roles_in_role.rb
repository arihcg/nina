class AddRolesInRole < ActiveRecord::Migration
  def self.up
    Role.create name: 'normal', description: 'this is the Normal users description'
    Role.create name: 'admin', description: 'this is the Administrators description'
    Role.create name: 'manager', description: 'this is the Managers description'
    end
end
