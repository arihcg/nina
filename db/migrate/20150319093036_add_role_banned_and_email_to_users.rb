class AddRoleBannedAndEmailToUsers < ActiveRecord::Migration
  def change
    add_reference :users, :role, index: true, default: 1
    add_column :users, :banned, :boolean, default: false
    add_column :users, :email, :string
  end
end
