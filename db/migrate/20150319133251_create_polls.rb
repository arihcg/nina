class CreatePolls < ActiveRecord::Migration
  def change
    create_table :polls do |t|
      t.string :title
      t.text :description
      t.datetime :start
      t.datetime :end
      t.integer :participants, default: 0

      t.timestamps
    end
  end
end
