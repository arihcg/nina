class AddCreatorToPoll < ActiveRecord::Migration
  def change
    add_column :polls, :creator_id, :string
  end
end
