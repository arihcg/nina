class AddLinksToCandidate < ActiveRecord::Migration
  def change
    add_column :candidates, :link1, :string
    add_column :candidates, :link2, :string
  end
end
