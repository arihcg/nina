class CreateCandidates < ActiveRecord::Migration
  def change
    create_table :candidates do |t|
      t.string :name
      t.string :description
      t.references :poll, index: true

      t.timestamps
    end
  end
end
