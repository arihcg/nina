Rails.application.routes.draw do

  resources :polls
  resources :candidates
  get 'candidates/new/:poll_id'   => 'candidates#new', as: :new_poll_candidate

  get 'admin/index'               => 'admin#index', as: :admin
  get 'admin/ban/:id'             => 'admin#ban', as: :ban
  get 'admin/release/:id'         => 'admin#release', as: :release
  get 'admin/makeAdmin/:id'       => 'admin#makeAdmin', as: :make_admin
  get 'admin/makeNormal/:id'      => 'admin#makeNormal', as: :make_normal
  get 'admin/makeManager/:id'     => 'admin#makeManager', as: :make_manager
  delete 'admin/destroy/:id'      => 'admin#destroy', as: :destroy

  devise_for :users

  root 'election#index'
  get 'election/index'
  get 'election/view/:poll_id'      => 'election#view', as: :election_view
  get 'election/vote/:candidate_id' => 'election#vote', as: :vote

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
